import React, {Component} from 'react';
import './Minesweeper.css';

function getColor(number) {
    const colors = [];
    colors.push('black');           // 0
    colors.push('dodgerblue');      // 1
    colors.push('darkgreen');       // 2
    colors.push('red');             // 3
    colors.push('rebeccapurple');   // 4
    colors.push('saddlebrown');     // 5
    colors.push('darkorange');      // 6
    colors.push('darkcyan');        // 7
    colors.push('darkslategray');   // 8

    const color = colors[number];
    if (color !== undefined) {
        return color;
    }

    return colors[number % colors.length];
}

function getBlockKey(columnIndex, rowIndex) {
    return columnIndex + '_' + rowIndex;
}

function getBlockColumnIndex(blockKey) {
    return parseInt(blockKey.split('_')[0], 10);
}

function getBlockRowIndex(blockKey) {
    return parseInt(blockKey.split('_')[1], 10);
}

class Block extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isActive: false,
            isMine: false,
            isShotPoint: false,
            mineCount: 0,
        };
    }

    placeMine() {
        this.setState({
            isMine: true
        });
    }

    updateMineCount(num) {
        this.setState({
            mineCount: num
        });
    }

    activeBlock() {
        this.setState({
            isActive: true
        });
    }

    labelShotPoint() {
        this.setState({
            isShotPoint: true
        });
    }

    clickBlock() {
        this.props.onClick(this.props.blockKey);
        this.setState({
            isActive: true
        });
    }

    render() {
        return (
            <button className='block' disabled={this.state.isActive}
                    style={{
                        color: getColor(this.state.mineCount),
                        background: this.state.isShotPoint ? 'red' : ''
                    }}
                    onClick={() => this.clickBlock()}>
                {this.state.isActive ? (this.state.isMine ? '⚫' : (this.state.mineCount > 0 && this.state.mineCount)) : ''}
            </button>
        );
    }
}

class Ground extends Component {
    constructor(props) {
        super(props);

        this.blockRefs = [];
        this.lookupActivedBlocks = [];
        this.clickBlock = this.clickBlock.bind(this);
    }

    componentDidMount() {
        this.generateMines();
    }

    clickBlock(blockKey) {
        const stateBlockRefs = this.blockRefs;
        const selectedBlockRef = stateBlockRefs[blockKey];
        if (selectedBlockRef.current.state.isMine) {
            selectedBlockRef.current.labelShotPoint();
        }
        this.checkSurroundingBlocks(blockKey);
    }

    /*
    *
    * c = columnIndex
    * r = rowIndex
    *
    *                   top
    *       [c-1,r-1] [c,r-1] [c+1,r-1]
    *  left [ c-1,r ] [ c,r ] [ c+1,r ] right
    *       [c-1,r+1] [c,r+1] [c+1,r+1]
    *                  bottom
    *
    * */
    checkSurroundingBlocks(blockKey) {
        const stateBlockRefs = this.blockRefs;
        const selectedBlockRef = stateBlockRefs[blockKey];

        selectedBlockRef.current.activeBlock();
        this.lookupActivedBlocks[selectedBlockRef.current.props.blockIndex] = null;
        if (selectedBlockRef.current.state.isMine) {
            // display all
            this.lookupActivedBlocks.forEach((value) => {
                if (value !== null) {
                    this.checkSurroundingBlocks(value);
                }
            });
            return;
        }

        const columnIndex = getBlockColumnIndex(blockKey);
        const rowIndex = getBlockRowIndex(blockKey);

        const range = 3; // detection range
        const end = Math.floor(range / 2);
        const start = -end;

        let mineCount = 0;
        let surroundingBlockKeys = [];
        for (let c = start; c <= end; c++) {
            for (let r = start; r <= end; r++) {
                const surroundingBlockKey = getBlockKey(columnIndex + c, rowIndex + r);
                const surroundingBlockRef = stateBlockRefs[surroundingBlockKey];
                if (surroundingBlockRef === undefined || surroundingBlockKey === blockKey) {
                    continue;
                }

                if (surroundingBlockRef.current.state.isMine) {
                    mineCount++;
                }

                surroundingBlockKeys.push(surroundingBlockKey);
            }
        }

        selectedBlockRef.current.updateMineCount(mineCount);

        if (mineCount === 0) {
            for (let i = 0; i < surroundingBlockKeys.length; i++) {
                const surroundingBlockKey = surroundingBlockKeys[i];
                const surroundingBlockRef = stateBlockRefs[surroundingBlockKey];
                if (this.lookupActivedBlocks[surroundingBlockRef.current.props.blockIndex] !== null) {
                    this.checkSurroundingBlocks(surroundingBlockKey);
                }
            }
        }
    }

    renderBlockRows() {
        const columns = this.props.columns;
        const rows = this.props.rows;

        let blockIndex = 0;
        let blockRows = [];
        for (let rowIndex = 0; rowIndex < rows; rowIndex++) {
            let blocks = [];
            for (let columnIndex = 0; columnIndex < columns; columnIndex++) {
                const blockKey = getBlockKey(columnIndex, rowIndex);
                const block = <Block columnIndex={columnIndex} rowIndex={rowIndex}
                                     blockIndex={blockIndex} blockKey={blockKey} key={blockKey}
                                     ref={this.blockRefs[blockKey] = React.createRef()}
                                     onClick={this.clickBlock}/>;
                blocks.push(block);
                this.lookupActivedBlocks[blockIndex] = blockKey;
                blockIndex++;
            }
            blockRows.push(<div className="block-row" key={rowIndex}>{blocks}</div>);
        }

        return blockRows;
    }

    generateMines() {
        const mines = this.props.mines;
        const columns = this.props.columns;
        const rows = this.props.rows;
        const totalBlocks = this.props.columns * this.props.rows;
        const blockRefs = this.blockRefs;

        let tempMineIndexes = [];
        // init tempMineIndexes
        for (let rowIndex = 0; rowIndex < rows; rowIndex++) {
            for (let columnIndex = 0; columnIndex < columns; columnIndex++) {
                const blockKey = columnIndex + '_' + rowIndex;
                tempMineIndexes.push(blockKey);
            }
        }

        // generate mine indexes randomly, the time complexity is O(m)
        for (let m = 0; m < mines; m++) {
            const randomMineIndex = Math.floor((Math.random() * (totalBlocks - m)) + m);
            const tempMineIndex = tempMineIndexes[m];
            // swap values
            tempMineIndexes[m] = tempMineIndexes[randomMineIndex];
            tempMineIndexes[randomMineIndex] = tempMineIndex;
        }

        // place mines
        for (let m = 0; m < mines; m++) {
            const selectedBlockRef = blockRefs[tempMineIndexes[m]];
            if (selectedBlockRef === undefined) {
                break;
            }
            selectedBlockRef.current.placeMine();
        }

    }

    calculateGroundWidth() {
        return (this.props.columns * 30) + "px";
    }

    render() {
        return (
            <div className="ground" style={{width: this.calculateGroundWidth()}}>
                {this.renderBlockRows()}
            </div>
        );
    }
}


class Minesweeper extends Component {
    render() {
        return (
            <div className="minesweeper">
                <Ground columns={15} rows={20} mines={100}/>
            </div>
        );
    }
}

export default Minesweeper;
